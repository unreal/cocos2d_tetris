#include "cocos2d.h"
#include "CCEGLView.h"
#include "AppDelegate.h"
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "StringMenu.h"
#include "CallMethod.h"
#include "TestSprite.h"
#include "AnimationTest.h"
#include "AnimationLayer.h"
#include "ActionTest.h"
#include "OrbitCamera.h"
#include "GridActionTest.h"
#include "KeyTest.h"
#include "TraceEx.h"
#include "SceneManager.h"
#include "StartScene.h"
#include "StartBackgroundLayer.h"
#include "StartUILayer.h"
using namespace CocosDenshion;

USING_NS_CC;

AppDelegate::AppDelegate()
{
	TraceEX::Trace("=========AppDelegate:" , "AppDelegate");
}

AppDelegate::~AppDelegate()
{
	TraceEX::Trace("=========AppDelegate:" , "~AppDelegate");
    SimpleAudioEngine::end();
}

bool AppDelegate::applicationDidFinishLaunching()
{
	TraceEX::Trace("=========AppDelegate:" , "Launching");
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());

    // turn on display FPS
    pDirector->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);




/**���Դ���
	CCScene* mScene =CCScene::create();
	std::function<void ()> method1=std::function<void ()>(&CallMethod::method1);
	CCSize size = CCDirector::sharedDirector()->getWinSize();
	StringMenu * stringMenu =new StringMenu(
		CCPoint(size.width/2 ,size.height*0.9),
		25,
		10,
		StringMenu::STRING_MENU_STYLE_VERTICAL);
	stringMenu->autorelease();

	StringMenuItem item1 = StringMenuItem("test Transition , Scence , Layer ,etc..." , method1);
	TestSprite testSprite = TestSprite();
	auto method2 = std::bind(&TestSprite::showSpriteScene,testSprite);
	StringMenuItem item2 = StringMenuItem("Enter Sprite Scence" , method2);
	AnimationTest animationTest = AnimationTest();
	auto animation = std::bind(&AnimationTest::showScene,animationTest);
	StringMenuItem item3 = StringMenuItem("animation etc." , animation);
	ActionTest actionTest = ActionTest();
	auto actionMethod = std::bind(&ActionTest::showScene , actionTest);
	StringMenuItem item4 = StringMenuItem("actionTest" , actionMethod);
	OrbitCamera orbitTest =OrbitCamera();
	auto camera = std::bind(&OrbitCamera::showScene , orbitTest);
	StringMenuItem item5 = StringMenuItem("OrbitCamera" , camera);
	GridActionTest grid = GridActionTest();
	auto gridScene = std::bind(&GridActionTest::showScene , grid);
	StringMenuItem item6 = StringMenuItem("Grid Test" , gridScene);
	KeyTest keyTst = KeyTest();
	auto keyBoard = std::bind(&KeyTest::showScene , keyTst);
	StringMenuItem item7 = StringMenuItem("KeyBoard Test" , keyBoard);
	stringMenu->addMenuItem(item1);
	stringMenu->addMenuItem(item2);
	stringMenu->addMenuItem(item3);
	stringMenu->addMenuItem(item4);
	stringMenu->addMenuItem(item5);
	stringMenu->addMenuItem(item6);
	stringMenu->addMenuItem(item7);

	stringMenu->update();
	mScene->addChild(stringMenu);
    pDirector->runWithScene(mScene);
	*/

	//����˹������Ϸ
	CCScene* pStartScene = StartScene::create();
	StartBackgroundLayer* pB=(StartBackgroundLayer*)pStartScene->getChildByTag(10010);
	StartUILayer* pU=(StartUILayer*)pStartScene->getChildByTag(10020);
	SceneManager::turnToScene(pStartScene);
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CCDirector::sharedDirector()->stopAnimation();

    SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CCDirector::sharedDirector()->startAnimation();

    SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
