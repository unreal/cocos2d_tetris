#pragma once
#include "cocos2d.h"
class GridActionTest{
public:
	GridActionTest();
	~GridActionTest();
	void showScene();
private:
	void static actionGrid(cocos2d::CCLayer* pLayer);

	void callback(cocos2d::CCObject* sender);
};