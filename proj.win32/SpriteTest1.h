#pragma once
#include "BasicSprite.h"
#include <string>

class SpriteTest1 : public BasicSprite
{
public:
    SpriteTest1();
    virtual std::string title();

    void addNewSpriteWithCoords(CCPoint p);
    void ccTouchesEnded(CCSet* touches, CCEvent* event);
};