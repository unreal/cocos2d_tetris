#pragma once
#include "cocos2d.h"
class ActionTest{
public:
	ActionTest();
	~ActionTest();
	void  showScene();

private:
	void static actionInstant(cocos2d::CCLayer* pLayer);
	void static actionInterval(cocos2d::CCLayer* pLayer);
	void static actionEase(cocos2d::CCLayer* pLayer);
	void static actionFollow(cocos2d::CCScene* pScene , cocos2d::CCLayer* pLayer);
	void static actionCallBack(cocos2d::CCLayer* pLayer);
	void  callback1();
	void static actionLoad(cocos2d::CCLayer* pLayer);
	void static actionManagerStop(cocos2d::CCLayer* pLayer);
	void static actionManagerResume(cocos2d::CCLayer* pLayer);



	static int flag; 

};