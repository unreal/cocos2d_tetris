#include "BlockItem.h"
#include "ResourcePath.h"
#include "ConstantConfig.h"
#include "TraceEX.h"
#include "Convert.h"
using namespace cocos2d;
bool BlockItem::init(){
	//默认是30 ， 先这么弄
	CCRect rect = CCRect(0,0,BlockItem::BLOCK_SIZE,BlockItem::BLOCK_SIZE);
	_mSprite=CCSprite::create(RES_IMAGE_BLOCKITEM.c_str(),rect);
	
	//设置小方块节点大小 以及节点位置，方便以后旋转啥的  将_sprite加入child之后，就以此节点为主，而不是sprite了。
	this->setContentSize(CCSize(BlockItem::BLOCK_SIZE , BlockItem::BLOCK_SIZE));
	this->setAnchorPoint(ccp(0.5f , 0.5f));//精灵锚点和节点锚点要统一。
	this->_mSprite->setAnchorPoint(ccp(0.5f , 0.5f));//便于从中间计算
	this->ignoreAnchorPointForPosition(true);
	this->_mSprite->ignoreAnchorPointForPosition(true);

	this->addChild(this->_mSprite);//之前没有添加,添加测试看是否可行！！ 可行
	return true;
}
void BlockItem::updateActualPos(){
	CCPoint pos((this->_pos.X)*BLOCK_SIZE,(this->_pos.Y)*BLOCK_SIZE);
	//this->_mSprite->ignoreAnchorPointForPosition(true);//不按照锚点写位置
	//this->_mSprite->setPosition(pos);//只负责相对位置

	//节点位置
	this->setPosition(pos);
}

BlockPos BlockItem::getCurPos(){
	//此Sprite的世界坐标。(以0.5，0.5为锚点时)
	CCPoint wordPos=this->convertToWorldSpaceAR(ccp( 0 ,0 ));

	int worldX =(int)wordPos.x;
	int worldY =(int)wordPos.y;
	
	//TraceEX::Trace("*******************获取全局位置时精灵世界坐标：" ,
	//	Convert::ToString(worldX)+","+Convert::ToString(worldY));
	if((wordPos.x-worldX)>=0.5){
		worldX+=1;
	}
	if((wordPos.y-worldY)>=0.5){
		worldY+=1;
	}
	int newPosX = ((worldX-BlockItem::BLOCK_SIZE/2)-(int)GRID_START_POS.x)/BlockItem::BLOCK_SIZE;//左下角横坐标。
	int newPosY = ((worldY-BlockItem::BLOCK_SIZE/2)-(int)GRID_START_POS.y)/BlockItem::BLOCK_SIZE;//左下角横坐标。
	//TraceEX::Trace("新位置 ：",Convert::ToString(newPosX)+","+Convert::ToString(newPosY));
	//可能由于锚点 会有些偏移量。
	return BlockPos(newPosX,newPosY);
}

void BlockItem::setPos(BlockPos position){
	this->_pos = position;
}

void BlockItem::setPos(int posX ,int posY){
	this->setPos(BlockPos(posX , posY));
}

BlockPos BlockItem::getPos(){
	return this->_pos;
}

int BlockItem::getPosX(){
	return this->_pos.X;
}
int BlockItem::getPosY(){
	return this->_pos.Y;
}
void BlockItem::setPosX(int posX){
	this->_pos.X = posX;
}
void BlockItem::setPosY(int posY){
	this->_pos.Y = posY;
}
CCSprite* BlockItem::getSprite(){
	return this->_mSprite;
}