#pragma once
#include "cocos2d.h"
class SceneManager{
public :
	/**
	*描述：转到某场景，若没有runningScene，则启动此场景
	*参数：
		  pScene：要转到的场景
	**/
	void static turnToScene(cocos2d::CCScene *pScene);
};