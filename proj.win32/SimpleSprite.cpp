#include "SimpleSprite.h"

void SimpleSprite::showSpriteBatch(){
	CCSpriteBatchNode* batchNode = CCSpriteBatchNode::create("grossini_dance_atlas.png", 50);  
        int idx = CCRANDOM_0_1() * 1400 /100;  
        int x = (idx % 5) * 85;  
        int y = (idx / 5) * 121;  
        CCSprite* sprite = CCSprite::createWithTexture(batchNode->getTexture(),CCRectMake(x,y,85,121));  
              
        CCSize size = CCDirector::sharedDirector()->getWinSize();      
                  
        sprite->setPosition(ccp(size.width/2, size.height/2));  
  
        batchNode->addChild(sprite);   
  
}