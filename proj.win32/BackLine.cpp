#pragma once
#include "BackLine.h"

BackLine::BackLine(){}
BackLine::~BackLine(){}

bool BackLine::init(){
	this->_lineSerial = 0;//默认是第0行。
	return true;
}

BackLine* BackLine::create(int lineSerial){
	BackLine* pRet =new BackLine();
	if(pRet){
		pRet->_lineSerial = lineSerial;
		return pRet;
	}
	CC_SAFE_DELETE(pRet);
	return nullptr;
}


void BackLine::addBlockItem(BlockItem* item){
	this->addChild(item);//同时同步到child里
	this->_lineItems.push_back(item);
}

std::list<BlockItem*> BackLine::getLineItems(){
	return this->_lineItems;
}


int BackLine::getLineSerial(){
	return this->_lineSerial;
}
void BackLine::setLineSerial(int lineSerial){
	this->_lineSerial = lineSerial;
}
