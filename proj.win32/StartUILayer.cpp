#include "StartUILayer.h"
#include "StringMenu.h"
#include "StringMenuItem.h"
#include "SceneManager.h"
#include "GameScene.h"
#include "AlertMessage.h";
#include "StartScene.h"
using namespace cocos2d;
StartUILayer::StartUILayer(){};
StartUILayer::~StartUILayer(){};
bool StartUILayer::init(){
	CCSize size = CCDirector::sharedDirector()->getWinSize();
	StringMenu * stringMenu =new StringMenu(
		CCPoint(size.width/2 ,size.height*0.7),
		25,
		10,
		StringMenu::STRING_MENU_STYLE_VERTICAL);
	stringMenu->autorelease();

	auto startGame = std::bind(&StartUILayer::startGameScene);
	auto message = std::bind(&AlertMessage::alert,"abc","def");
	StringMenuItem item1 = StringMenuItem("turn to game" , startGame);
	StringMenuItem item2 = StringMenuItem("alert messagebox" , message);
	stringMenu->addMenuItem(item1);
	stringMenu->addMenuItem(item2);
	stringMenu->update();
	this->addChild(stringMenu);
	return true;
}
void StartUILayer::initMenu(){
	
}

//ת����Ϸ����
void StartUILayer::startGameScene(){
	CCScene* gameScene = GameScene::create();
	SceneManager::turnToScene(gameScene);
}