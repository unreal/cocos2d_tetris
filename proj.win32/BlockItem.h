#pragma once
#include "cocos2d.h"
#include "TerisHelp.h"
/**
方块类
**/
class BlockItem : public cocos2d::CCNode{
public:
	const static int BLOCK_SIZE=30;
private:
	BlockPos _pos;//格子位置,相对于背景格子
	cocos2d::CCSprite* _mSprite;
public:
	CREATE_FUNC(BlockItem);
	bool init();

public:
	int getPosX();
	int getPosY();
	void setPosX(int posX);
	void setPosY(int posY);
	/**
	*@comment 获取cos的相对位置
	**/
	BlockPos getPos();
	void setPos(BlockPos position);
	void setPos(int posX , int posY);
	/**
	*@comment 在此位置上根据pos（格子位置） 计算相对位置
	*相对于父类起点的位置
	**/
	void updateActualPos();
	cocos2d::CCSprite* getSprite();
	
	/**
	**@comment 获取此方块在背景格子中的位置。
	** 以GRID_START_POS为坐标起点。
	**/
	BlockPos getCurPos();
};
