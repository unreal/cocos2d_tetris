#include "Block.h"
#include "TraceEX.h"
#include "Convert.h"
#include "ConstantConfig.h"

using namespace cocos2d;
using namespace std;
Block::Block(){}
Block::~Block(){}
const CCPoint Block::START_POSITION = ccp(60.f, 90.f);//初始位置在
const string B_TWO_TWO[]={"11" , "11" };//1代表有，0代表没有每个字符串表示一行。
const string B_FOUR[]={"0000","1111","0000"};
const string B_LEFT_TWO_TWO[]={"011" , "110" };
const string B_RIGHT_TWO_TWO[]={"110" , "011" };
const string B_MIDDLE_ONE_THREE[]={"111" , "010" };;
const string B_LEFT_ONE_THREE[]={"111","100"};
const string B_RIGHT_ONE_THREE[]={"111","001"};

const std::vector<std::string> Block::VEC_B_TWO_TWO(B_TWO_TWO ,B_TWO_TWO+2);
const std::vector<std::string> Block::VEC_B_FOUR(B_FOUR ,B_FOUR+3);
const std::vector<std::string> Block::VEC_B_LEFT_TWO_TWO(B_LEFT_TWO_TWO , B_LEFT_TWO_TWO+2);
const std::vector<std::string> Block::VEC_B_RIGHT_TWO_TWO(B_RIGHT_TWO_TWO , B_RIGHT_TWO_TWO+2);
const std::vector<std::string> Block::VEC_B_MIDDLE_ONE_THREE(B_MIDDLE_ONE_THREE , B_MIDDLE_ONE_THREE+2);
const std::vector<std::string> Block::VEC_B_LEFT_ONE_THREE(B_LEFT_ONE_THREE , B_LEFT_ONE_THREE+2);
const std::vector<std::string> Block::VEC_B_RIGHT_ONE_THREE(B_RIGHT_ONE_THREE , B_RIGHT_ONE_THREE+2);

const string Block::IS_BLOCKITEM("1");//1代表有小方块
bool Block::init(){
	this->_pos=BlockPos(BLOCK_DEFAULT_POS);
	this->ignoreAnchorPointForPosition(true);
	this->setAnchorPoint(ccp(0.5 ,0.5));//设置锚点
	this->setPosition(this->START_POSITION);
	this->updateBlockPos();
	this->resetItemWithRelease(Block::VEC_B_TWO_TWO);
	return true;
}

void Block::resetItemWithRelease(const vector<string> preShape){
	//清空之前的子item  Block
	//for(int ix = 0; ix <(this->_vecItems.size()) ; ix++){
	//	this->_vecItems[ix]->getSprite()->removeFromParentAndCleanup(true);
	//	this->_vecItems[ix]->release();
	//}
	this->removeAllChildrenWithCleanup(true);
	this->resetItemNoRelease(preShape);
}

void Block::resetItemNoRelease(const vector<string> preShape){
	this->removeAllChildren();//从本实例里清除子类
	this->_vecItems.clear();
	//设置大方块content size大小：第一个字符串表示的格子数*预设宽度
	int blockWidth=preShape[0].size()*BlockItem::BLOCK_SIZE;
	this->setContentSize(CCSize(blockWidth , blockWidth));
	string strLine;
	for(int ix = 0 ; ix <preShape.size() ; ix++){
		strLine=preShape[ix];
		TraceEX::Trace("size:"+Convert::ToString(strLine.size()),"length:"+Convert::ToString(strLine.length()));
		for(int jx=0 ; jx<strLine.size() ; jx++){
			//截取对应位置字符串，检查是否有小方块
			//TraceEX::Trace("sub：",strLine.substr(jx,1));
			if(strLine.substr(jx,1)==IS_BLOCKITEM){
				BlockItem* item = BlockItem::create();
				item->setTag(1000+ix*10+jx);
				//TraceEX::Trace("位置：" ,Convert::ToString(jx+1)+","+Convert::ToString(ix+1));
				item->setPos(jx, ix);//格子位置（相对 大方块左下）
				item->retain();
				item->updateActualPos();
				//添加进子项
				_vecItems.push_back(item);
				this->addChild(item);//直接添加item可用。
			}
		}
	}
}


void Block::rotateBlock(int extent){
	//旋转大方块
	this->setRotation(this->getRotation()+90*extent);
	this->updateBlockItemPos();
		
}



void Block::updateBlockPos(){
	//算出现在 大方块 起点
	CCPoint pos =this->getPosition();
	BlockPos blockPos = this->getPos();
	pos.x = blockPos.X*BlockItem::BLOCK_SIZE;
	pos.y = blockPos.Y*BlockItem::BLOCK_SIZE;
	//大方块位置应该是有2dx维护的，不用我维护
	this->setPosition(pos);//更新大方块位置
	this->updateBlockItemPos();
}


void Block::updateBlockItemPos(){
	for(int ix=0 ;ix<_vecItems.size() ; ix++){
		BlockPos newPos = _vecItems[ix]->getCurPos();
		 _vecItems[ix]->setPos(newPos);
	}
}


//不做判断，只是移动
void Block::moveHorizontal(int offset){
	BlockPos oldPos = this->_pos;
	//TraceEX::Trace("横向移动 offset",Convert::ToString(offset));
	this->_pos.X+=offset;
	this->updateBlockPos();
}

void Block::moveVertical(int offset){
	BlockPos oldPos = this->_pos;
	//TraceEX::Trace("纵向移动 offset",Convert::ToString(offset));
	this->_pos.Y+=offset;
	this->updateBlockPos();
}




/**
	*@comment 检测是否与左边界 碰撞（已经贴上）
	**/
bool Block::isCollideLeft(){
	BlockItem* pItem;
	BlockPos backPos;
	for(int ix = 0;ix<(this->_vecItems).size() ; ix++){
		pItem =	this->_vecItems[ix];
		backPos = pItem->getCurPos();
		if(backPos.X<=0){
			return true;
		}
	}
	return false;
}
	/**
	*@comment 检测是否与右边界 碰撞（已经贴上）
	**/
bool Block::isCollideRight(){
	BlockItem* pItem;
	BlockPos backPos;
	for(int ix = 0;ix<(this->_vecItems).size() ; ix++){
		pItem =	this->_vecItems[ix];
		backPos = pItem->getCurPos();
		if(backPos.X>=(ConstantConfig::GRID_WIDTH-1)){
			return true;
		}
	}
	return false;
}
	/**
	*@comment 检测是否与下边界 碰撞（已经贴上）
	**/
bool Block::isCollideBottom(){
	BlockItem* pItem;
	BlockPos backPos;
	for(int ix = 0;ix<(this->_vecItems).size() ; ix++){
		pItem =	this->_vecItems[ix];
		backPos = pItem->getCurPos();
		//TraceEX::Trace("碰撞检测 下边缘 item pos Y:" , Convert::ToString(backPos.Y));
		if(backPos.Y<=0){
			return true;
		}
	}
	//TraceEX::Trace("==========================","");
	return false;
}



bool Block::isTouchItemBottom(BlockItem* destItem){
	for(int ix = 0 ; ix<this->_vecItems.size() ; ix++){
		if((_vecItems[ix]->getPosX() == destItem->getPosX())&&//x轴一样时
		   (_vecItems[ix]->getPosY() == (destItem->getPosY()+1))//y轴大1
		  ){
			  return true;
		}
	}
	return false;
}

bool Block::isTouchItemLeft(BlockItem* destItem){
		for(int ix = 0 ; ix<this->_vecItems.size() ; ix++){
			if((_vecItems[ix]->getPosY() == destItem->getPosY())&&
			   (_vecItems[ix]->getPosX() == (destItem->getPosX()+1))//destItem在方块左边
				){
					return true;
				}
	}
	return false;
}

bool Block::isTouchItemRight(BlockItem* destItem){
		for(int ix = 0 ; ix<this->_vecItems.size() ; ix++){
			if((_vecItems[ix]->getPosY() == destItem->getPosY())&&
			   (_vecItems[ix]->getPosX() == (destItem->getPosX()-1))//destItem在方块右边
				){
					return true;
				}
	}
	return false;
}






std::vector<BlockItem*> Block::getItems(){
	return this->_vecItems;
}

BlockPos Block::getPos(){
	return this->_pos;
}
int Block::getPosX(){
	return this->_pos.X;
}
int Block::getPosY(){
	return this->_pos.Y;
}

void Block::setPos(BlockPos newPos){
	this->_pos = newPos;
	this->updateBlockPos();
}

void Block::setPosX(int posX){
	this->setPos(BlockPos(posX , this->getPosY()));
}

void Block::setPosY(int posY){
	this->setPos(BlockPos(this->getPosX() , posY));
}

