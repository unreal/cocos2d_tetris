#pragma once
#include "cocos2d.h"
#include "BlockItem.h"
#include <vector>
#include <list>
/**
**背景格子 背景行
**/
class BackLine :public cocos2d::CCNode{
private:
	int _lineSerial;
	std::list<BlockItem*> _lineItems;


public:
	BackLine();
	~BackLine();

	CREATE_FUNC(BackLine);
	//初始化
	bool init();
	/**
	**@comment 创建方法
	**lineSerial 行号
	**/
	static BackLine* create(int lineSerial);
	
	/**
	**@comment 添加一个小方块
	**/
	void addBlockItem(BlockItem* item);

	/**
	**@comment 获取整行 item。
	**/
	std::list<BlockItem*> getLineItems();
	
	/**
	**@comment 获得背景行 行号
	**/
	int getLineSerial();
	/**
	**@comment 设置背景行 行号
	**/
	void setLineSerial(int lineSerial);
};