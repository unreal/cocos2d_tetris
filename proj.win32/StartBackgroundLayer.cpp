#include "StartBackgroundLayer.h"
#include "ResourcePath.h"
using namespace cocos2d;

StartBackgroundLayer::StartBackgroundLayer(){}
StartBackgroundLayer::~StartBackgroundLayer(){}

bool StartBackgroundLayer::init(){
	CCSize size = CCDirector::sharedDirector()->getWinSize();
	CCSprite* pFlag =CCSprite::create(RES_IMAGE_STARTSCENE_BACKGROUND.c_str());
	pFlag->setPosition(ccp(size.width/2 , size.height/2));
	this->addChild(pFlag);
	return true;
}