#pragma once
#include "cocos2d.h"
#include <string>
using namespace cocos2d;
class BasicSprite : public CCLayer
{
protected:
    std::string    m_strTitle;

public:
    BasicSprite(void);
    ~BasicSprite(void);

    virtual std::string title();
    virtual std::string subtitle();
    virtual void onEnter();

    void restartCallback(CCObject* pSender);
    void nextCallback(CCObject* pSender);
    void backCallback(CCObject* pSender);
};