#include "MainMenu.h"
#include "BasicScene.h"
#include "SpriteScene.h"

//static CCPoint s_tCurPos = CCPointZero;
/**
**创建测试场景
**返回的是精灵场景
**/
//static BasicScene* CreateBasicScene(int nIdx)
//{
//    CCDirector::sharedDirector()->purgeCachedData();
//
//	BasicScene* pScene = NULL;
//    pScene = new SpriteScene(); 
//    return pScene;
//}

/**
**创建菜单 ccmenu
**/
MainMenu::MainMenu()
: m_tBeginPos(CCPointZero)
{
    // add close menu
    CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
         "CloseNormal.png",
         "CloseSelected.png",
		this, 
		menu_selector(MainMenu::closeCallback) );
    CCMenu* pMenu =CCMenu::create(pCloseItem, NULL);

    pMenu->setPosition( CCPointZero );
    pCloseItem->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 20, 20));
    // add menu items for tests
    m_pItemMenu = CCMenu::create();
    CCLabelTTF* label = CCLabelTTF::create("sprite", "Arial", 34);
   
    CCMenuItemLabel* pMenuItem = CCMenuItemLabel::create(
		label,
		this, 
		menu_selector(MainMenu::menuCallback));

    m_pItemMenu->addChild(pMenuItem,10000);
	pMenuItem->setPosition( ccp(CCDirector::sharedDirector()->getWinSize().width-400, CCDirector::sharedDirector()->getWinSize().height-20));
	
	
    m_pItemMenu->setContentSize(CCSize((float)(300), (float)(300)));
    m_pItemMenu->setPosition(CCPoint((float)(0), (float)(0)));
    addChild(m_pItemMenu);

    setTouchEnabled(false);

   addChild(pMenu, 1);

}

MainMenu::~MainMenu()
{
}
/**
**菜单回调
**转到新场景
**/
void MainMenu::menuCallback(CCObject * pSender)
{
    // get the userdata, it's the index of the menu item clicked
    CCMenuItem* pMenuItem = (CCMenuItem *)(pSender);
    int nIdx = pMenuItem->getZOrder() - 10000;

    // create the test scene and run it
	BasicScene* pScene = new SpriteScene(); 
	pScene = new SpriteScene(); 
    if (pScene)
    {
		pScene->runThisSecne();
        pScene->release();
    }
}

void MainMenu::closeCallback(CCObject * pSender)
{
    CCDirector::sharedDirector()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

