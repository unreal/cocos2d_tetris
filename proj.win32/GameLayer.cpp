#include "GameLayer.h"
#include "ResourcePath.h"
#include "ConstantConfig.h"
#include "input.h"
#include "TraceEX.h"
#include "Convert.h"
#include "StringMenu.h"
#include "StringMenuItem.h"
#include "BackLine.h"
using namespace cocos2d;
using namespace std;
GameLayer::GameLayer(){}
GameLayer::~GameLayer(){}

bool GameLayer::init(){

	CCSize size = CCDirector::sharedDirector()->getWinSize();

	//键盘监听
	CCDirector::sharedDirector()->getOpenGLView()->setAccelerometerKeyHook(GameLayer::keyboardHook);

	//菜单
	StringMenu * stringMenu =new StringMenu(
			CCPoint(size.width-200.f ,size.height-40.f),
		20,
		15,
		StringMenu::STRING_MENU_STYLE_VERTICAL);
		stringMenu->autorelease();
		auto switchFour =std::bind(&GameLayer::switchState , Block::VEC_B_FOUR); 
		auto leftTT =std::bind(&GameLayer::switchState , Block::VEC_B_LEFT_TWO_TWO);
		auto rightTT =std::bind(&GameLayer::switchState , Block::VEC_B_RIGHT_TWO_TWO);
		auto leftO =std::bind(&GameLayer::switchState , Block::VEC_B_LEFT_ONE_THREE);
		auto middleO =std::bind(&GameLayer::switchState , Block::VEC_B_MIDDLE_ONE_THREE);
		auto rightO =std::bind(&GameLayer::switchState , Block::VEC_B_RIGHT_ONE_THREE);
		auto mTT =std::bind(&GameLayer::switchState , Block::VEC_B_TWO_TWO);

	StringMenuItem item1=StringMenuItem("to Four",switchFour);
	StringMenuItem item2=StringMenuItem("to leftTT",leftTT);
	StringMenuItem item3=StringMenuItem("to rightTT",rightTT);
	StringMenuItem item4=StringMenuItem("to leftO",leftO);
	StringMenuItem item5=StringMenuItem("to middleO",middleO);
	StringMenuItem item6=StringMenuItem("to rightO",rightO);
	StringMenuItem item7=StringMenuItem("to mTT",mTT);

	stringMenu->addMenuItem(item1);
	stringMenu->addMenuItem(item2);
	stringMenu->addMenuItem(item3);
	stringMenu->addMenuItem(item4);
	stringMenu->addMenuItem(item5);
	stringMenu->addMenuItem(item6);
	stringMenu->addMenuItem(item7);
	stringMenu->update();

		//操作菜单
	StringMenu * operateMenu =new StringMenu(
			CCPoint(size.width-100.f ,size.height-40.f),
		20,
		15,
		StringMenu::STRING_MENU_STYLE_VERTICAL);
		operateMenu->autorelease();
	
		this->_gameBackGround =BackBlock::create();
		this->_gameBackGround->setPosition(GRID_START_POS);//需要给背景格子加上网格起点
		this->_gameBackGround->setTag(TAG_GAME_BACKBLOCK);
		this->addChild(this->_gameBackGround);//将背景方块类添加进游戏层


	_curBlcok=Block::create();
	this->addChild(_curBlcok);//将当前方块添加进游戏层
	_curBlcok->setTag(TAG_GAME_BLOCK_CUR);

		auto clockwise =std::bind(&Block::rotateBlock , _curBlcok ,1); 
		auto anticlockwise =std::bind(&Block::rotateBlock , _curBlcok ,-1); 
		StringMenuItem item11=StringMenuItem("clockwise",clockwise);
		StringMenuItem item12=StringMenuItem("anticlockwise",anticlockwise);
		operateMenu->addMenuItem(item11);
		operateMenu->addMenuItem(item12);
		operateMenu->update();

	this->addChild(stringMenu);
	this->addChild(operateMenu);

	//更新函数 调用默认更新函数
	schedule(schedule_selector(GameLayer::gameUpdate),1.f);  
	return true;
}

void GameLayer::switchState(const std::vector<std::string> shapeState){
	CCScene* pScene= CCDirector::sharedDirector()->getRunningScene();
	GameLayer* gameLayer =(GameLayer*)pScene->getChildByTag(ConstantConfig::GAMESCENE_GAMELAYER_TAG);
	Block* blockCur=(Block*)gameLayer->getChildByTag(TAG_GAME_BLOCK_CUR);
	blockCur->resetItemWithRelease(shapeState);
}

void GameLayer::gameUpdate(float dt){
	TraceEX::Trace("进入更新循环 dt:",Convert::ToString(dt));
	CCScene* pScene= CCDirector::sharedDirector()->getRunningScene();
	GameLayer* gameLayer =(GameLayer*)pScene->getChildByTag(ConstantConfig::GAMESCENE_GAMELAYER_TAG);
	this->_curBlcok=(Block*)gameLayer->getChildByTag(TAG_GAME_BLOCK_CUR);


	bool isTouchTop = this->_gameBackGround->isTouchTop(this->_curBlcok);
	//若与底部未碰撞
	if(!(_curBlcok->isCollideBottom()) &&!isTouchTop){
		_curBlcok->moveVertical(-1);
	}

	
	 
	 TraceEX::Trace("是否与顶层  接触：",Convert::ToString(isTouchTop));
	//若与底部已碰撞  或与背景格子碰撞
	if(this->_curBlcok->isCollideBottom()||isTouchTop
	  ){
		//将block的值赋给背景格，并重置。	
		vector<BlockItem*> items = _curBlcok->getItems();
		for(int ix =0 ; ix<items.size() ; ix++){
			BlockPos curPos = items[ix]->getCurPos();
			items[ix]->setPos(curPos);//重置之后 这个位置会变，具体是获取curPos时候获得的世界坐标会变，估计跟清空出父类有关。
			items[ix]->setRotation(_curBlcok->getRotation());
		}

		BlockPos blockPos = _curBlcok->getPos();
		//重置也会引起 方块回归原位，操。
		_curBlcok->resetItemNoRelease(Block::VEC_B_LEFT_TWO_TWO);//先重置，再遍历，否则会报父类冲突
		_curBlcok->setPos(BlockPos(5,20));//重置

		//将每个方块放入对应的backLine，再存入背景。
		BlockItem* item;
		for(int ix=0 ; ix<items.size() ; ix++){
			item = items[ix];
			item->updateActualPos();
			this->_gameBackGround->addBlockItem(item);
			TraceEX::Trace("小方块位置：" , Convert::ToString(item->getPos().X)+","+Convert::ToString(item->getPos().Y));
		}
		
	}

}

void GameLayer::keyboardHook(UINT message,WPARAM wParam, LPARAM lParam){
//	TraceEX::Trace("key message:::::::",Convert::ToString(message));
//	TraceEX::Trace("key wParam:::::::",Convert::ToString(wParam));
	CCScene* pScene= CCDirector::sharedDirector()->getRunningScene();
	GameLayer* gameLayer =(GameLayer*)pScene->getChildByTag(ConstantConfig::GAMESCENE_GAMELAYER_TAG);
	Block* blockCur=(Block*)gameLayer->getChildByTag(TAG_GAME_BLOCK_CUR);
	//游戏背景格子
	BackBlock* backBlock = (BackBlock*)gameLayer->getChildByTag(TAG_GAME_BACKBLOCK);


	TraceEX::Trace("大方块位置 操作前：",Convert::ToString(blockCur->getPos().X)+","+Convert::ToString(blockCur->getPos().Y));
	//按键被按下
	if(message==WM_KEYDOWN){
		if(wParam==VK_UP||wParam==VK_NUMPAD5){
			blockCur->rotateBlock(1);
		}
		if(wParam==VK_NUMPAD8){
			blockCur->moveVertical(1);
		}
		if(wParam==VK_DOWN||wParam==VK_NUMPAD2){//下移
		    bool isTouchTop = backBlock->isTouchTop(blockCur);
			if(!(blockCur->isCollideBottom()) && !isTouchTop ){
				blockCur->moveVertical(-1);
			}
		}
		if(wParam==VK_LEFT||wParam==VK_NUMPAD4){//左移
			bool isTouchBackRight = backBlock->isTouchRight(blockCur);//是否接触到背景格右侧
			//若没碰撞
			if(!(blockCur->isCollideLeft()) && !isTouchBackRight){
				blockCur->moveHorizontal(-1);
			}
		}
		if(wParam==VK_RIGHT||wParam==VK_NUMPAD6){//右移
			bool isTouchBackLeft = backBlock->isTouchLeft(blockCur);//是否接触到背景格左侧
			if(!(blockCur->isCollideRight()) && !isTouchBackLeft){
				blockCur->moveHorizontal(1);
			}
		}
	}
	TraceEX::Trace("大方块位置 操作后：",Convert::ToString(blockCur->getPos().X)+","+Convert::ToString(blockCur->getPos().Y));
}