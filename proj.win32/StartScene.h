#pragma once
#include "cocos2d.h"

/**
*描述：起始的菜单场景
*
**/
class StartScene : public cocos2d::CCLayer{
public:
	StartScene();
	~StartScene();
	static cocos2d::CCScene* create();
};