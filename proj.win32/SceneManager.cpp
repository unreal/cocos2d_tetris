#include "SceneManager.h"
using namespace cocos2d;


void SceneManager::turnToScene(CCScene* pScene){
	CCLog("retainCount in turnToScene:%d",pScene->retainCount());
	 CCDirector* pDirector = CCDirector::sharedDirector();
	 CCScene* curScene = pDirector->getRunningScene();
	 if(curScene == NULL) {
	     pDirector->runWithScene(pScene);
	 }
	 else {
	     pDirector->replaceScene(pScene);
	 }
}

