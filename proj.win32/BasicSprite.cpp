#include "BasicSprite.h"
#include "SpriteScene.h"
//------------------------------------------------------------------
//
// BasicSprite
//
//------------------------------------------------------------------

BasicSprite::BasicSprite(void)
{
}

BasicSprite::~BasicSprite(void)
{
}

std::string BasicSprite::title()
{
    return "No title";
}

std::string BasicSprite::subtitle()
{
    return "";
}

void BasicSprite::onEnter()
{
    CCLayer::onEnter();

    CCSize s = CCDirector::sharedDirector()->getWinSize();

    CCLabelTTF* label = CCLabelTTF::create(title().c_str(), "Arial", 28);
    addChild(label, 1);
    label->setPosition( ccp(s.width/2, s.height-50) );

    std::string strSubtitle = subtitle();
    if( ! strSubtitle.empty() ) 
    {
        CCLabelTTF* l = CCLabelTTF::create(strSubtitle.c_str(), "Thonburi", 16);
        addChild(l, 1);
        l->setPosition( ccp(s.width/2, s.height-80) );
    }    

   CCMenuItemImage *item1 = CCMenuItemImage::create("b1.png", "b2.png", this, menu_selector(BasicSprite::backCallback) );
   CCMenuItemImage *item2 = CCMenuItemImage::create("r1.png","r2.png", this, menu_selector(BasicSprite::restartCallback) );
    CCMenuItemImage *item3 = CCMenuItemImage::create("f1.png", "f2.png", this, menu_selector(BasicSprite::nextCallback) );

    CCMenu *menu = CCMenu::create(item1, item2, item3, NULL);

    menu->setPosition(CCPointZero);
    item1->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width-220,
										CCDirector::sharedDirector()->getWinSize().height-500));
    item2->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width-150,
										CCDirector::sharedDirector()->getWinSize().height-500));
	item3->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width-80,
										CCDirector::sharedDirector()->getWinSize().height-500));
    addChild(menu, 1);    
}

void BasicSprite::restartCallback(CCObject* pSender)
{
    CCScene* s = new SpriteScene();
  //  s->addChild(restartSpriteTestAction()); 

    CCDirector::sharedDirector()->replaceScene(s);
    s->release();
}

void BasicSprite::nextCallback(CCObject* pSender)
{
    CCScene* s = new SpriteScene();
//	CCLayer* pLayer =new Sprite1();
//	pLayer->autorelease();
 //   s->addChild( nextSpriteTestAction() );
    CCDirector::sharedDirector()->replaceScene(s);
    s->release();
}

void BasicSprite::backCallback(CCObject* pSender)
{
    CCScene* s = new SpriteScene();
 //   s->addChild( backSpriteTestAction() );
    CCDirector::sharedDirector()->replaceScene(s);
    s->release();
} 