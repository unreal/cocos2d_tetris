#include "BasicScene.h"

BasicScene::BasicScene(bool bPortrait)
{
    CCScene::init();
}

//进入场景默认生成返回主页面的菜单
void BasicScene::onEnter()
{
    CCScene::onEnter();
    CCLabelTTF* label = CCLabelTTF::create("MainMenu", "Arial", 20);
    CCMenuItemLabel* pMenuItem = CCMenuItemLabel::create(label, this, menu_selector(BasicScene::MainMenuCallback));
    CCMenu* pMenu =CCMenu::create(pMenuItem, NULL);
    pMenu->setPosition( CCPointZero );
    pMenuItem->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 50, 50));	
    addChild(pMenu, 1);
}


/**
**返回主菜单 回调函数
**返回testController场景
**/
void BasicScene::MainMenuCallback(CCObject* pSender)
{
    CCScene* pScene = CCScene::create();
	CCLayer* pLayer =nullptr;// new TestController();
    pLayer->autorelease();
    pScene->addChild(pLayer);
    CCDirector::sharedDirector()->replaceScene(pScene);
}
