#pragma once
#include "cocos2d.h"
#include "TerisHelp.h"
#include "BlockItem.h"
#include <vector>
class Block : public cocos2d::CCNode{
public:
	CREATE_FUNC(Block);
	bool init();

	Block();
	~Block();

public:
	//需要一个左下角的点，一个右上角的点。用来解决边界问题。
	/**
	*@comment 重置方块形状，并释放所属小方块
	*preShape 预设形状
	**/
	void resetItemWithRelease(const std::vector<std::string> preShape);

	/**
	*@comment 重置方块形状，不释放小方块(当小方块还需要使用的时候调用此方法)
	**/
	void resetItemNoRelease(const std::vector<std::string> preShape);
	


	/**
	*@comment 横向移动几个格子的位置
	*@offset 为负时向左移动 为正时向右移动
	**/
	void moveHorizontal(int offset);
	/**
	*@comment 竖向移动几格格子的位置
	*@offset 为负时向下移动 为正时向上移动
	**/
	void moveVertical(int offset);

	/**
	*@comment 旋转方块（同时更新各个小方块位置等信息）
	*@extent 旋转幅度 表示旋转90度的倍数 （正值为顺时针，负值为逆时针）
	**/
	void rotateBlock(int extent);
private:
	/**
	*@comment 更新大方块位置（包括内部小方块）
	**/;
	void updateBlockPos();

	/**
	*@comment 更新其中包括的所有小方块位置，每次大方块有动作都应当更新。
	**/
	void updateBlockItemPos();
private:
	static const cocos2d::CCPoint START_POSITION;//方块位置 起始位置
	BlockPos  _pos;//大方块位置 相对于背景格子 左下角
	std::vector<BlockItem*> _vecItems;//方块列表   此大格子有的
public:
	std::vector<BlockItem*> getItems();
	BlockPos  getPos();
	int		  getPosX();
	int		  getPosY();
	void	  setPos(BlockPos newPos);
	void	  setPosX(int posX);
	void	  setPosY(int posY);
	/**
	*@comment 检测是否与左边界 碰撞（已经贴上）
	**/
	bool isCollideLeft();
	/**
	*@comment 检测是否与右边界 碰撞（已经贴上）
	**/
	bool isCollideRight();
	/**
	*@comment 检测是否与下边界 碰撞（已经贴上）
	**/
	bool isCollideBottom();

	/**
	*@comment 是否与destItem在 大方块底部 碰撞（已经贴上）
	**/
	bool isTouchItemBottom(BlockItem* destItem);
	/**
	*@comment 是否与destItem在 大方块的右侧 接触
	**/
	bool isTouchItemRight(BlockItem* destItem);
	/**
	*@comment 是否与destItem在 大方块的左侧 接触
	**/
	bool isTouchItemLeft(BlockItem* destItem);

public:

	static const std::vector<std::string> VEC_B_TWO_TWO;
	static const std::vector<std::string> VEC_B_FOUR;
	static const std::vector<std::string> VEC_B_LEFT_TWO_TWO;
	static const std::vector<std::string> VEC_B_RIGHT_TWO_TWO;
	static const std::vector<std::string> VEC_B_MIDDLE_ONE_THREE;
	static const std::vector<std::string> VEC_B_LEFT_ONE_THREE;
	static const std::vector<std::string> VEC_B_RIGHT_ONE_THREE;

	//标记某个位置是否有 BlockItem
	static const std::string IS_BLOCKITEM;
};
