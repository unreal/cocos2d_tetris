#include "GridActionTest.h"
#include "StringMenu.h"
#include "StringMenuItem.h"
#include "AlertMessage.h"
using namespace cocos2d;
GridActionTest::GridActionTest(){
}
GridActionTest::~GridActionTest(){
}

void GridActionTest::showScene(){
	CCDirector *pDirector = CCDirector::sharedDirector();
	pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
	CCSize size = pDirector->getWinSize();
	CCScene * pScene = CCScene::create();
	CCSprite* pFlag =CCSprite::create("flag.png");
	pFlag->setPosition(ccp(50,size.height - 50));
	pScene->addChild(pFlag);
	CCLayer* pLayer = CCLayer::create();
	pScene->addChild(pLayer);
	pDirector->replaceScene(pScene);

	auto actionGrid = std::bind(GridActionTest::actionGrid , pLayer);
	StringMenuItem item1 =StringMenuItem("action Grid" , actionGrid);


	StringMenu* menu = new StringMenu(
		CCPoint(size.width*0.8 ,size.height*0.9),
		25,
		10,
		StringMenu::STRING_MENU_STYLE_VERTICAL);
		menu->autorelease();
		pScene->addChild(menu);
		menu->addMenuItem(item1);
		menu->update();
}

void GridActionTest::actionGrid(CCLayer* pLayer){
	pLayer->removeAllChildrenWithCleanup(true);
	CCDirector::sharedDirector()->setDepthTest(false);  
	CCSprite* pWater =  CCSprite::create("water.png");
	pWater->setTag(1020);
	pLayer->addChild(pWater);
	CCSize size = CCDirector::sharedDirector()->getWinSize();
	CCPoint centerPos = ccp(size.width*0.5 , size.height*0.5);
	pWater->setPosition(centerPos);
	CCRipple3D* ripple = CCRipple3D::create(15,CCSizeMake(32,24) , centerPos ,400,10 ,160);
	CCWaves* waves=CCWaves::create(15 , CCSizeMake(32,24) ,15 ,16 ,true , true );
	CCWaves3D* wave3D = CCWaves3D::create(15 , CCSizeMake(32,24) ,15 , 16);
	CCWavesTiles3D* waveTile3D = CCWavesTiles3D::create(3 , CCSizeMake(16 ,12) , 8 , 60);

	
	CCFiniteTimeAction*  action = CCSequence::create(
       waveTile3D,  
       CCCallFuncN::create(pLayer, callfuncN_selector(GridActionTest::callback)), NULL);  
	pWater->runAction(action);
}

void GridActionTest::callback(CCObject* sender){
	CCNode* s2 =(CCNode*)sender;
	//CCNode* s2 = pLayer->getChildByTag(1020);
	AlertMessage::alert("bbbbb","a");
	
	if( s2->getGrid() != NULL){
				AlertMessage::alert("b","a");
		s2->setGrid(NULL);
	}

	if (s2->numberOfRunningActions() == 0 && s2->getGrid() != NULL){

	   s2->setGrid(NULL);
	}
     
}