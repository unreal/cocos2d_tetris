#pragma once
#include "cocos2d.h"
using namespace cocos2d;
using namespace std;

//测试场景类
class BasicScene : public CCScene
{
public: 
    BasicScene(bool bPortrait = false);
    virtual void onEnter();

    virtual void runThisSecne() = 0;

    //主菜单回调函数
    virtual void MainMenuCallback(CCObject* pSender);
};


