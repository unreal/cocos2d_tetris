#pragma once
#include "cocos2d.h"
#include <functional>
/**
*菜单项目
**/
class StringMenuItem:public cocos2d::CCObject{
public:
	/**
	*@comment 新生成一个文字菜单项
	*@param itemTiltle 菜单项文字标题
	*@param callbackFunc 菜单项回调方法
	**/
	StringMenuItem(std::string itemTitle , std::function<void ()> callbackFunc);
	~StringMenuItem();
	/**
	* @comment 返回菜单项标题
	**/
	std::string getItemTitle(){
		return this->itemTitle;
	}
	/**
	*@comment 返回菜单项回调方法
	**/
	std::function<void ()> getCallbackFunc(){
		return this->callbackFunc;
	}
	//std::function<void(cocos2d::CCScene*)> getCallback(){
	//	return this->callback;
	//}

private:
	std::string itemTitle;//菜单标题
	std::function<void ()> callbackFunc;//菜单回调方法
	//std::function<void(cocos2d::CCScene*)> callback;
};