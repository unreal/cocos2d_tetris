#pragma once
#include "cocos2d.h"
class AnimationTest {
public:
	AnimationTest();
	~AnimationTest();
	void showScene();
private:
	void static sceneUpdate(cocos2d::CCScene* pScene , cocos2d::CCLayer* pLayer);
	void static showAnimationScene(cocos2d::CCScene* pScene);
};