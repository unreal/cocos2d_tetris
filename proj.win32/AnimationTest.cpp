#include "AnimationTest.h"
#include "StringMenu.h"
#include "StringMenuItem.h"
#include "AlertMessage.h"
#include "AnimationLayer.h"
using namespace cocos2d;
AnimationTest::AnimationTest(){
}
AnimationTest::~AnimationTest(){
}

void AnimationTest::showScene(){
	
	CCDirector *pDirector = CCDirector::sharedDirector();
	pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
	CCScene * pScene = CCScene::create();

	CCLayer* pLayer = CCLayer::create();
	pScene->addChild(pLayer , 1012);
	
	
	pDirector->replaceScene(pScene); 
	 CCSize size = CCDirector::sharedDirector()->getWinSize();
		StringMenu * stringMenu =new StringMenu(
			CCPoint(size.width-130.f ,size.height-40.f),
		20,
		15,
		StringMenu::STRING_MENU_STYLE_VERTICAL);
	stringMenu->autorelease();
	
	pScene->addChild(stringMenu,1200);
	auto sceneUpdate = std::bind(AnimationTest::sceneUpdate , pScene , pLayer);
	auto showAnimationScene = std::bind(AnimationTest::showAnimationScene , pScene);
	StringMenuItem item1 = StringMenuItem("sceneUpdate", sceneUpdate);
	StringMenuItem item2 = StringMenuItem("show AniamtionScene", showAnimationScene);
	stringMenu->addMenuItem(item1);
	stringMenu->addMenuItem(item2);
	stringMenu->update();
}

void AnimationTest::sceneUpdate(CCScene* pScene , CCLayer* pLayer){
	
	CCSprite* pFlagSprite= CCSprite::create("flag.png");
	 CCSize size = CCDirector::sharedDirector()->getWinSize();
	 pFlagSprite->setPosition(ccp(size.width/2, size.height/2));  
	pLayer->addChild(pFlagSprite,1001);
}

void AnimationTest::showAnimationScene(CCScene* pScene){

	AnimationLayer* layer =AnimationLayer::create();
	pScene->addChild(layer);
}
