#include "TestSprite.h"
#include "AlertMessage.h"
#include "StringMenu.h"
#include "StringMenuItem.h"
#include "TraceEX.h"
#include "Convert.h"

TestSprite::TestSprite(){
	std::cout<<"TestSprite TestSprite()"<<std::endl;
	//this->pSpriteScene = CCScene::create();//这里即便初始化，在showSpriteScene中也会出错没值。

}
TestSprite::~TestSprite(){
}

void TestSprite::showSpriteScene(){
	
	CCDirector *pDirector = CCDirector::sharedDirector();
	pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
	CCScene * pSpriteScene = CCScene::create();
	 CCSize size = CCDirector::sharedDirector()->getWinSize();
	 TraceEX::TraceExFloat(size.width , "width:%f");
	 TraceEX::TraceExFloat(size.height , "height:%f");
		StringMenu * stringMenu =new StringMenu(
			CCPoint(size.width-130.f ,size.height-40.f),
		20,
		15,
		StringMenu::STRING_MENU_STYLE_VERTICAL);
	stringMenu->autorelease();


	// 创建图片精灵  
    //CCSprite* pSprite = CCSprite::create("flag.png");    
    // 设置图片精灵的位置
    //pSprite->setPosition(ccp(size.width/2, size.height/2));  
	//pSpriteScene->addChild(pSprite,1031);

			    // 创建图片精灵  
    CCSprite* pSprite1 = CCSprite::create("flag.png");    
    // 设置图片精灵的位置
    pSprite1->setPosition(CCPoint(size.width-600.f ,size.height-400.f));  
//	pSpriteScene->addChild(pSprite1 ,1032);
	CCSprite* pSprite2 = CCSprite::create("flag.png");    
    // 设置图片精灵的位置
    pSprite2->setPosition(CCPoint(size.width-600.f ,size.height-80.f));  
	pSpriteScene->addChild(pSprite2 ,1032);

	static CCLayer* pSrpiteLayer = CCLayer::create();
	pSrpiteLayer->setTag(11022);
	pSrpiteLayer->addChild(pSprite1 , 1032);
	pSpriteScene->addChild(pSrpiteLayer , 1012);
//	pSpriteScene->addChild(pSrpiteLayer);
	static auto funcAddSprite = std::bind(&TestSprite::addSprite,pSpriteScene , pSrpiteLayer);
	auto funcAddSprite2 = std::bind(&TestSprite::addSprite2,pSpriteScene , pSrpiteLayer);
	auto funcAddSprite3 = std::bind(&TestSprite::addSprite3,pSpriteScene  , pSrpiteLayer);
	auto funcAddSprite4 = std::bind(&TestSprite::addSprite4,pSpriteScene  , pSrpiteLayer);
	auto funcAddSprite5 = std::bind(&TestSprite::addSprite5,pSpriteScene  , pSrpiteLayer);
	StringMenuItem item1 = StringMenuItem("test text font" , funcAddSprite);
	StringMenuItem item2 = StringMenuItem("SpriteBatchNode" , funcAddSprite2);
	StringMenuItem item3 = StringMenuItem("test AliasTex" , funcAddSprite3);
	StringMenuItem item4 = StringMenuItem("test Sprite frame" , funcAddSprite4);
	StringMenuItem item5 = StringMenuItem("test CCSpriteFrameCache" , funcAddSprite5);
	
	stringMenu->addMenuItem(item1);
	stringMenu->addMenuItem(item2);
	stringMenu->addMenuItem(item3);
	stringMenu->addMenuItem(item4);
	stringMenu->addMenuItem(item5);
	stringMenu->update();
	pSpriteScene->addChild(stringMenu,1099);
	pDirector->replaceScene(pSpriteScene); 
}

void TestSprite::addSprite(CCScene * pSpriteScene , CCLayer* pSrpiteLayer){

	pSrpiteLayer->removeAllChildren();
	//设置背景色
	CCLayerColor* layer = CCLayerColor::create(ccc4(0xFF,0x80,0x00,0xFF),1024,768); 
	pSpriteScene->addChild(layer);
    // 设置图片精灵的位置
    CCSize size = CCDirector::sharedDirector()->getWinSize();

	
	//创建CCLabelTTF标签对象  
	CCSize blockSize = CCSizeMake(size.width/2 - 200, 50);  
	CCLabelTTF* ttfL = CCLabelTTF::create("Buxton Sketch","Buxton Sketch",30 , blockSize ,  kCCTextAlignmentLeft );
	ttfL->setPosition(ccp(size.width/2-200, size.height/2));
	CCLabelTTF* ttfL2 = CCLabelTTF::create("这是隶书","LiSu", 30, blockSize ,kCCTextAlignmentLeft);
	ttfL2->setPosition(ccp(size.width/2-200, size.height/2-50));
	CCLabelTTF* ttfL3 = CCLabelTTF::create("左对齐xxx","STXihei", 30, blockSize,kCCTextAlignmentLeft);
	ttfL3->setPosition(ccp(size.width/2-200, size.height/2-100));

	//好像垂直 位置 默认是top
	CCLabelTTF* ttfM = CCLabelTTF::create("Buxton  Top","Buxton Sketch", 30, blockSize,kCCTextAlignmentCenter, kCCVerticalTextAlignmentBottom);
	ttfM->setPosition(ccp(size.width/2, size.height/2));
	CCLabelTTF* ttfM2 = CCLabelTTF::create("隶书Center","LiSu", 30, blockSize,kCCTextAlignmentCenter , kCCVerticalTextAlignmentTop);
	ttfM2->setPosition(ccp(size.width/2, size.height/2-50));
	CCLabelTTF* ttfM3 = CCLabelTTF::create("对齐Button","LiSu", 30, blockSize,kCCTextAlignmentCenter, kCCVerticalTextAlignmentTop);
	ttfM3->setPosition(ccp(size.width/2, size.height/2-100));

	//blockSize = CCSizeMake(size.width/2 - 200, 50);  
	CCLabelTTF* ttfR = CCLabelTTF::create("Buxton Sketc","Buxton Sketch", 30 , blockSize ,kCCTextAlignmentRight  , kCCVerticalTextAlignmentCenter);
	ttfR->setPosition(ccp(size.width/2+200, size.height/2));
	CCLabelTTF* ttfR2 = CCLabelTTF::create("这是隶书右X","LiSu", 30 , blockSize ,kCCTextAlignmentRight);
	ttfR2->setPosition(ccp(size.width/2+200, size.height/2-50));
	CCLabelTTF* ttfR3 = CCLabelTTF::create("右对齐XXX","LiSu", 30 , blockSize ,kCCTextAlignmentRight);
	ttfR3->setPosition(ccp(size.width/2+200, size.height/2-100));

	//Top
	CCLabelTTF* ttfT = CCLabelTTF::create("Buxton SketchTTT","Buxton Sketch", 20);
	ttfT->setPosition(ccp(size.width/2, size.height/2+250));
	CCLabelTTF* ttfT2 = CCLabelTTF::create("微软雅黑TTT","Microsoft YaHei", 20);
	ttfT2->setPosition(ccp(size.width/2, size.height/2+220));
	CCLabelTTF* ttfT3 = CCLabelTTF::create("雅黑粗体","Microsoft YaHei UI Bold", 20);
	ttfT3->setPosition(ccp(size.width/2, size.height/2+190));
	CCLabelTTF* ttfT4 = CCLabelTTF::create("这是楷体","STKaiti", 20);
	ttfT4->setPosition(ccp(size.width/2, size.height/2+160));
	CCLabelTTF* ttfT5 = CCLabelTTF::create("彩云字体","STCaiyun", 20);
	ttfT5->setPosition(ccp(size.width/2, size.height/2+130));
	ttfT5->setColor(ccRED);
	CCLabelTTF* ttfT6 = CCLabelTTF::create("幼圆字体","YouYuan", 20);
	ttfT6->setPosition(ccp(size.width/2, size.height/2+100));


	pSrpiteLayer->addChild(ttfL , 1002);
	pSrpiteLayer->addChild(ttfL2 , 1003);
	pSrpiteLayer->addChild(ttfL3 , 1003);

	pSrpiteLayer->addChild(ttfM , 1002);
	pSrpiteLayer->addChild(ttfM2 , 1003);
	pSrpiteLayer->addChild(ttfM3 , 1003);

	pSrpiteLayer->addChild(ttfR , 1002);
	pSrpiteLayer->addChild(ttfR2 , 1003);
	pSrpiteLayer->addChild(ttfR3 , 1003);

	pSrpiteLayer->addChild(ttfT , 1004);
	pSrpiteLayer->addChild(ttfT2 , 1005);
	pSrpiteLayer->addChild(ttfT3 , 1005);
	pSrpiteLayer->addChild(ttfT4 , 1005);
	pSrpiteLayer->addChild(ttfT5 , 1005);
	pSrpiteLayer->addChild(ttfT6 , 1005);







	   //创建Atlas标签对象  
        CCLabelAtlas* label1 = CCLabelAtlas::create("123 Test", "fonts/tuffy_bold_italic-charmap.plist");  
		label1->setContentSize(blockSize);
        //设置位置  
        label1->setPosition(ccp(0 , size.height/2+190));  
        //设置透明度  
        label1->setOpacity(200);
		
        CCLabelAtlas *label2 = CCLabelAtlas::create("0123456789", "fonts/tuffy_bold_italic-charmap.plist");  
        label2->setPosition(ccp(0 , size.height/2+150));  
   //     label2->setOpacity(32);  
        label2->setColor( ccRED );  


		pSrpiteLayer->addChild(label1 , 1005);
		pSrpiteLayer->addChild(label2 , 1005);


		

		  //使用字体资源文件，来创建一个CCLabelBMFont对象  
        CCLabelBMFont* bmpLabel1 = CCLabelBMFont::create("Test",  "fonts/bitmapFontTest2.fnt");  
        //设置字体锚点并添加至显示  
        bmpLabel1->setAnchorPoint( ccp(0,0) );  
		bmpLabel1->setScale(2);
         CCLabelBMFont *bmpLabel2 = CCLabelBMFont::create("Test", "fonts/bitmapFontTest2.fnt");  
        // testing anchors  
        bmpLabel2->setAnchorPoint( ccp(0.5f, 0.5f) );  
        bmpLabel2->setColor( ccRED );  

        CCLabelBMFont* bmpLabel3 = CCLabelBMFont::create("Test", "fonts/bitmapFontTest2.fnt");  
        // testing anchors  
        bmpLabel3->setAnchorPoint( ccp(0.5f,1) );  

		 CCLabelBMFont* bmpLabel4 = CCLabelBMFont::create("a中华人民共和国x", "fonts/bitmapFontChinese.fnt");  
		 CCLabelBMFont* bmpLabel5 = CCLabelBMFont::create("a我猜这几个字没有x", "fonts/bitmapFontChinese.fnt");  
		 CCLabelBMFont* bmpLabel6 = CCLabelBMFont::create("a街对面是大纪念碑x", "fonts/bitmapFontChinese.fnt");
		 bmpLabel6->setColor(ccRED);
        // testing anchors  
		//bmpLabel3->setAnchorPoint( ccp(0.5f,1) );  
		CCLabelBMFont* bmpLabel7 = CCLabelBMFont::create("a这些字绝对有x", "fonts/chinese.fnt");  
		CCLabelBMFont* bmpLabel8 = CCLabelBMFont::create("a你猜猜这是啥x", "fonts/chinese.fnt"); 
		
		
    
		bmpLabel8->setColor(ccBLUE);

		bmpLabel1->setPosition(ccp(size.width/2-300 , size.height/2-190));
		bmpLabel2->setPosition(ccp(size.width/2-300 , size.height/2-220));
		bmpLabel3->setPosition(ccp(size.width/2-300 , size.height/2-250));

		bmpLabel4->setPosition(ccp(size.width/2 , size.height/2-220));
		bmpLabel5->setPosition(ccp(size.width/2 , size.height/2-250));
		bmpLabel6->setPosition(ccp(size.width/2 , size.height/2-280));

		bmpLabel7->setPosition(ccp(size.width/2+350 , size.height/2-280));
		bmpLabel8->setPosition(ccp(size.width/2+350 , size.height/2-310));
		
		pSpriteScene->addChild(bmpLabel1 , 1022);
		pSpriteScene->addChild(bmpLabel2 , 1022);
		pSpriteScene->addChild(bmpLabel3 , 1022);

		pSpriteScene->addChild(bmpLabel4 , 1023);
		pSpriteScene->addChild(bmpLabel5 , 1024);
		pSpriteScene->addChild(bmpLabel6 , 1024);

		pSpriteScene->addChild(bmpLabel7 , 1024);
		pSpriteScene->addChild(bmpLabel8 , 1024);



}
void TestSprite::addSprite2(CCScene * pSpriteScene , CCLayer* pSpriteLayer){
	pSpriteLayer->removeAllChildren();
	// 创建图片精灵  
	CCSprite* pSprite = CCSprite::create("flower.jpg",CCRectMake(0.f , 0.f  , 300.f ,1025.f));    
    // 设置图片精灵的位置
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    pSprite->setPosition(ccp(size.width/2, size.height/2));  

	
	
	CCSpriteBatchNode* batchNode = CCSpriteBatchNode::create("dance_man.png", 50);  
	for(int ix = 0 ; ix<9 ; ix++){
		CCSprite* pDance =  CCSprite::createWithTexture(batchNode->getTexture(),CCRectMake(ix*100,0,100,200));
		pDance->setPosition(ccp(30.f+ix*100, size.height/2));  
		
		//这个精灵特殊处理下
		if( ix == 4 ){
			pDance->setScaleX( 8);  
			pDance->setColor(ccRED); 
			pDance->setZOrder(50);
		}
		if(ix == 2){
			int order  = pDance->getZOrder();
			std::string str = Convert::ToString(order);
			//AlertMessage::alert(Convert::ToString(order),"2  zorder" );
		//	pDance->setZOrder(101);
		}
			if(ix == 6){
			int order  = pDance->getZOrder();
			std::string str = Convert::ToString(order);
			//AlertMessage::alert(Convert::ToString(order),"6  zorder" );
			//pDance->setZOrder(18);
		}
			pDance->setTag(ix);
			int tag  =  pDance->getTag();
			std::string title =Convert::ToString(ix) +"  Tag" ;
			//AlertMessage::alert(Convert::ToString(tag),title);
		batchNode->addChild(pDance);
	}
	pSpriteLayer->addChild(batchNode , 1056);
	pSpriteLayer->addChild(pSprite);
}


void  TestSprite::addSprite3(CCScene * pSpriteScene, CCLayer* pSpriteLayer){
	pSpriteLayer->removeAllChildren();
	CCSize size = CCDirector::sharedDirector()->getWinSize();
	CCSprite* pSprite = CCSprite::create("dance_man.png",CCRectMake(0.f , 0.f  , 100.f ,200.f));    
	//pSprite->getTexture()->setAliasTexParameters();
    pSprite->setPosition(ccp(size.width/2-60.f, size.height/2)); 
	pSprite->setScale(2.0f);
	CCSprite* pSprite2 = CCSprite::create("dance_man_copy.png",CCRectMake(0.f , 0.f  , 100.f ,200.f));    
	pSprite2->getTexture()->setAntiAliasTexParameters();
    pSprite2->setPosition(ccp(size.width/2+60.f, size.height/2)); 
	pSprite2->setScale(2.0f);
	pSpriteLayer->addChild(pSprite);
	pSpriteLayer->addChild(pSprite2);
}


void  TestSprite::addSprite4(CCScene * pSpriteScene, CCLayer* pSpriteLayer){
	pSpriteLayer->removeAllChildren();
	CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage("dance_man.png");
	//将动画帧放入此数组
	CCArray *animFrames = new CCArray(7);
	CCSprite* pSprite;
	for(int ix = 0 ; ix<7 ; ix++){
		CCSpriteFrame *frame= CCSpriteFrame::createWithTexture(texture , CCRectMake(ix*100,0,100,200));
		animFrames->addObject(frame);
		if(ix == 0){
			//用第一帧初始精灵
			pSprite = CCSprite::createWithSpriteFrame(frame);
			CCSize size = CCDirector::sharedDirector()->getWinSize();
			pSprite->setPosition(ccp(size.width/2-60.f, size.height/2)); 
		}	
	}
	CCAnimation *animation = CCAnimation::createWithSpriteFrames(animFrames,0.2);
	//animation->autorelease();
	//#3：使用animation生成一个动画动作animate 

	CCAnimate *animate = CCAnimate::create(animation);
	
	pSprite->runAction(CCRepeatForever::create(animate));//重复播放 
	pSpriteLayer ->addChild(pSprite);
}

void  TestSprite::addSprite5(CCScene * pSpriteScene, CCLayer* pSpriteLayer){
	pSpriteLayer->removeAllChildren();
	CCSpriteFrameCache* cache = CCSpriteFrameCache::sharedSpriteFrameCache();  
    
	cache->addSpriteFramesWithFile("animations/grossini.plist");
    cache->addSpriteFramesWithFile("animations/grossini_gray.plist", "animations/grossini_gray.png");
    cache->addSpriteFramesWithFile("animations/grossini_blue.plist", "animations/grossini_blue.png");
	
	
	
	CCArray* animFrames = CCArray::createWithCapacity(15);
	
	CCSprite* pSprite = CCSprite::createWithSpriteFrameName("grossini_dance_01.png");

	CCSpriteBatchNode* spritebatch = CCSpriteBatchNode::create("animations/grossini.png");
    spritebatch->addChild(pSprite);
	pSpriteLayer->addChild(spritebatch);
	CCSize size = CCDirector::sharedDirector()->getWinSize();
	pSprite->setPosition(ccp(size.width/2, size.height/2)); 
	char str[100] = {0};
	for(int i = 1; i < 15; i++) 
		{
			sprintf(str, "grossini_dance_%02d.png", i);
			CCSpriteFrame* frame = cache->spriteFrameByName( str );
			animFrames->addObject(frame);
		}
	 CCAnimation* animation = CCAnimation::createWithSpriteFrames(animFrames, 0.3f);
	 pSprite->runAction(CCRepeatForever::create( CCAnimate::create(animation)));
}