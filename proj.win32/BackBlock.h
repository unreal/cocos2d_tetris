#pragma once
#include "cocos2d.h"
#include "BlockItem.h"
#include "Block.h"
#include "BackLine.h"
#include <list>

/**
**背景格子 整体背景
**/
class BackBlock :public cocos2d::CCNode {
private:
	std::list<BackLine*> _listLines;//存放背景格子线的
	
private:
	/**
	**@comment 增加一行 背景格子
	**/
	void addBackLine(BackLine*);

public:
	BackBlock();
	~BackBlock();
	CREATE_FUNC(BackBlock);
	//初始化
	bool init();


	/**
	*@comment 检测背景与此大方块是否在 上层 有接触
	**/
	bool isTouchTop(Block*);
	/**
	**@comment 此大方块与背景在右侧是否接触 
	**/
	bool isTouchRight(Block*);
	/**
	**@comment 此大方块与背景在左侧是否接触
	**/
	bool isTouchLeft(Block*);

	/**
	**@comment 是否有完整待消除的行。
	**/
	bool isFullBlockLine();

	/**
	**@comment 根据行号获取此行。若没有，则返回一个新行。
	**/
	BackLine* getBackLine(int lineSerial);
	/**
	**@comment 获取完整待消除的行
	**/
	std::list<BackLine*> getFullBlockLine();
	/**
	*@comment 增加一个BlockItem到对应行。
	**/
	void addBlockItem(BlockItem*);

	/**
	**@comment 清除一行 背景格子
	**/
	void delBackLine(BackLine*);

};