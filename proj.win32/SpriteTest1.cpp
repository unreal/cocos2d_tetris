#include "SpriteTest1.h"


SpriteTest1::SpriteTest1()
{
    setTouchEnabled( true );
    CCSize s = CCDirector::sharedDirector()->getWinSize();
    addNewSpriteWithCoords( ccp(s.width/2, s.height/2) );
    
}

void SpriteTest1::addNewSpriteWithCoords(CCPoint p)
{
    int idx = (int)(CCRANDOM_0_1() * 1400.0f / 100.0f);
    int x = (idx%5) * 85;
    int y = (idx/5) * 121;
    
    
    CCSprite* sprite = CCSprite::create("grossini_dance_atlas.png", CCRectMake(x,y,85,121) );
    addChild( sprite );
    
    sprite->setPosition( ccp( p.x, p.y) );
    
    CCActionInterval* action;
    float random = CCRANDOM_0_1();
    
    if( random < 0.20 )
        action = CCScaleBy::create(3, 2);
    else if(random < 0.40)
        action = CCRotateBy::create(3, 360);
    else if( random < 0.60)
        action = CCBlink::create(1, 3);
    else if( random < 0.8 )
        action = CCTintBy::create(2, 0, -255, -255);
    else 
        action = CCFadeOut::create(2);
    CCActionInterval* action_back = action->reverse();
    CCActionInterval* seq = CCSequence::create( action, action_back, NULL );
    
    sprite->runAction( CCRepeatForever::create(seq) );
}

void SpriteTest1::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    CCSetIterator it;
    CCTouch* touch;

    for( it = touches->begin(); it != touches->end(); it++) 
    {
        touch = (CCTouch*)(*it);

        if(!touch)
            break;

        CCPoint location = touch->getLocation();
    
        addNewSpriteWithCoords( location );
    }
}

std::string SpriteTest1::title()
{
    return "Sprite (tap screen)";
}