#pragma once
#include "cocos2d.h"
#include "TerisHelp.h"
class ConstantConfig{
public:
	/* 开发时默认的屏幕大小 */
    static const int DEFAULT_SCREEN_WIDTH = 1024;
    static const int DEFAULT_SCREEN_HEIGHT = 768;

	static const int GRID_WIDTH=12;//游戏格子横排个数
	static const int GRID_HEIGHT=21;//游戏格子竖排个数
	
	static const cocos2d::CCPoint GRID_START_POS;

	static const int GAMESCENE_GAMELAYER_TAG=10010;//游戏场景->游戏层tag
};

static const cocos2d::CCPoint GRID_START_POS(60.f,90.f);
static const BlockPos BLOCK_DEFAULT_POS(5,21);//方块默认位置



//默认tag
static const int TAG_GAME_BLOCK_CUR = 100051;//当前运动的方块

static const int TAG_GAME_BACKBLOCK =100010;//当前游戏背景 格子类
