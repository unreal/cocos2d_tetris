#include "StartScene.h"
#include "StartBackgroundLayer.h"
#include "StartUILayer.h"
using namespace cocos2d;

CCScene* StartScene::create(){
	CCScene* pScene = CCScene::create();
	StartBackgroundLayer* pBackLayer = StartBackgroundLayer::create();
	pBackLayer->setTag(10010);
	pScene->addChild(pBackLayer,10);
	StartUILayer* uiLayer = StartUILayer::create();
	uiLayer->initMenu();
	uiLayer->setTag(10020);
	pScene->addChild(uiLayer , 100);

	StartBackgroundLayer* pB=(StartBackgroundLayer*)pScene->getChildByTag(10010);
	StartUILayer* pU=(StartUILayer*)pScene->getChildByTag(10020);
	return pScene;
}