#include "OrbitCamera.h"
#include "StringMenu.h"
#include "StringMenuItem.h"
#include "AlertMessage.h"
using namespace cocos2d;
OrbitCamera::OrbitCamera(){
}
OrbitCamera::~OrbitCamera(){
}
void OrbitCamera::showScene(){
	CCDirector *pDirector = CCDirector::sharedDirector();
	pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
	CCScene * pScene = CCScene::create();
	CCLayer* pLayer = CCLayer::create();
	pScene->addChild(pLayer , 1012);
	pDirector->replaceScene(pScene); 
	 CCSize size = CCDirector::sharedDirector()->getWinSize();
		StringMenu * stringMenu =new StringMenu(
			CCPoint(size.width-130.f ,size.height-40.f),
		20,
		15,
		StringMenu::STRING_MENU_STYLE_VERTICAL);
	stringMenu->autorelease();
	
	pScene->addChild(stringMenu,1200);
	auto orbit = std::bind(OrbitCamera::orbit,pScene);
	StringMenuItem item1 = StringMenuItem("orbit", orbit);
	stringMenu->addMenuItem(item1);
	//stringMenu->addMenuItem(item2);
	stringMenu->update();
}
void OrbitCamera::orbit(CCScene* pScene){
	
		CCSprite* pFlag1 =CCSprite::create("xiaoguai/12.png");
		CCSprite* pFlag2 =CCSprite::create("xiaoguai/13.png");
		CCSprite* pFlag3 =CCSprite::create("xiaoguai/14.png");
		   //获得尺寸大小  
        CCSize size = CCDirector::sharedDirector()->getWinSize();  
		pScene->addChild(pFlag1);
		pScene->addChild(pFlag2);
		pScene->addChild(pFlag3);
        //设置精灵的位置  
        pFlag1->setPosition( ccp(size.width/2, size.height/2));  
        pFlag2->setPosition( ccp(size.width/4, size.height/2));  
        pFlag3->setPosition( ccp(3 * size.width/4, size.height/2));  

	    CCActionInterval*  orbit1 = CCOrbitCamera::create(2,1, 0, 0, 180, 0, 0);  
        CCSequence*  action1 = CCSequence::create(  
        orbit1,  
        orbit1->reverse(),  
        NULL);  
  
        CCActionInterval*  orbit2 = CCOrbitCamera::create(2,1, 0, 0, 180, -45, 0);  
        CCSequence*  action2 = CCSequence::create(  
        orbit2,  
        orbit2->reverse(),  
        NULL);  
  
        CCActionInterval*  orbit3 = CCOrbitCamera::create(2,1, 0, 0, 180, 90, 0);  
        CCSequence*  action3 = CCSequence::create(  
        orbit3,  
        orbit3->reverse(),  
        NULL);  

	    pFlag1->runAction(CCRepeatForever::create(action1));  
        pFlag2->runAction(CCRepeatForever::create(action2));  
        pFlag3->runAction(CCRepeatForever::create(action3));  
      


		CCActionInterval*  move = CCMoveBy::create(3, ccp(100,-100));  
        CCActionInterval*  move_back = move->reverse();  
        CCSequence*  seq = CCSequence::create(move, move_back, NULL);  
        CCAction*  rfe = CCRepeatForever::create(seq);  

        pFlag1->runAction(rfe);  
        pFlag2->runAction(seq);  
        pFlag3->runAction((CCAction*)(rfe->copy()->autorelease()));  
  
}