#pragma once
#include <windows.h>  
#define KEY_DOWN(vk_code) (GetAsyncKeyState(vk_code) & 0x8000 ? 1 : 0)//按键按下
#define KEY_UP(vk_code) (GetAsyncKeyState(vk_code) & 0x8000 ? 0 : 1)//按键抬起
#define KEY_DOWN_ONCE(vk_code) (GetAsyncKeyState(vk_code) & 0x8001 ? 1 : 0)//按键按下
/**
*@comment 按键输入
**/
class KeyInput{
private:
	int _state;//按键状态
	int _interval;//间隔时间
public:
	KeyInput(){
		_state=0;
		_interval=0;
	}
	bool isPressOnce(int v2k_code){
	//	_state=KEY_DOWN(vk_code);
		GetAsyncKeyState(v2k_code);
		return true;
	}
};