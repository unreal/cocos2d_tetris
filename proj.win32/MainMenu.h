#pragma once
#include "cocos2d.h"
using namespace cocos2d;
using namespace std;

class MainMenu : public CCLayer
{
public:
    MainMenu();
    ~MainMenu();

    void menuCallback(CCObject * pSender);
    void closeCallback(CCObject * pSender);

private:
    CCPoint m_tBeginPos;
    CCMenu* m_pItemMenu;
};