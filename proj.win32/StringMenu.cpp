#include "StringMenu.h"
#include "CallMethod.h"
#include <memory>

using namespace std;
using namespace cocos2d;

/**
**创建菜单 ccmenu
**/
const string StringMenu::STRING_MENU_STYLE_VERTICAL("vertical");
const string StringMenu::STRING_MENU_STYLE_HORIZONTAL("horizontal");

StringMenu::StringMenu()
: m_BeginPos(CCPoint(20.f, 200.f)),
zOrder(10000),
m_fontSize(20),
m_range(10),
menuStyle(STRING_MENU_STYLE_VERTICAL)
{
    m_pMenu = CCMenu::create();
	m_pMenu->setPosition(m_BeginPos);
	addChild(m_pMenu);
    setTouchEnabled(false);
}

StringMenu::StringMenu(CCPoint beginPos , int fontSize , int range , string menuStyle)
:zOrder(10000)
{
	this->m_fontSize=fontSize;
	this->m_range = range;
	this->menuStyle = menuStyle;
	this->m_BeginPos= beginPos;
	m_pMenu = CCMenu::create();
	m_pMenu->setPosition(m_BeginPos);
	addChild(m_pMenu);
    setTouchEnabled(false);
}

StringMenu::~StringMenu()
{
}
void StringMenu::update(){
	for(int ix= 0 ;  ix<vecMenuList.size() ;  ix++){
    string itemTitle = vecMenuList[ix].getItemTitle();
	CCLabelTTF* label = CCLabelTTF::create(itemTitle.c_str(), "Arial", m_fontSize);
	float labelSize = label->getFontSize();
    CCMenuItemLabel* pMenuItem = CCMenuItemLabel::create(
		label,
		this, 
		menu_selector(StringMenu::menuCallback));
	
	//将每一个子菜单存入userData ，以便在回调中取值
	StringMenuItem* menuItemTemp=&vecMenuList[ix];
	pMenuItem->setUserData((void*)menuItemTemp); 
	
	CCPoint itemPos = ccp(0 , 0);//
	if(zOrder>10000){//不是第一个item
		//计算menuItem位置
		if(menuStyle==STRING_MENU_STYLE_VERTICAL){
				itemPos.y-=m_fontSize*ix;
				itemPos.y-=m_range*ix;
		}else if(menuStyle==STRING_MENU_STYLE_HORIZONTAL){			
			itemPos.x+=m_fontSize*((int)itemTitle.size()/2)*ix;
			itemPos.x+=m_range;

		}
	}
	pMenuItem->setPosition(itemPos);
	zOrder+=1;
    m_pMenu->addChild(pMenuItem,zOrder);
	}

}
/**
**添加一个菜单项
**/
void StringMenu::addMenuItem(StringMenuItem stringMenuItem){
	this->vecMenuList.push_back(stringMenuItem);
}

/**
**菜单回调
**/
void StringMenu::menuCallback(CCObject * pSender)
{

    CCMenuItem* pMenuItem = (CCMenuItem *)(pSender);
    int nIdx = pMenuItem->getZOrder() - 10000;
	//string* data = (string*)pMenuItem->getUserData();
	StringMenuItem* menuItem =(StringMenuItem*)(pMenuItem->getUserData());
	auto method=menuItem->getCallbackFunc();
	method();

}
