/**
*@Date 2013-8-24
*@Auther pascalkk
*@Company 甲骨文股份有限公司
**/
#pragma once
#include "cocos2d.h"
using namespace cocos2d;
using namespace std;
/**
*@content 精灵场景菜单类
*@date  2013-8-24
**/
class SpriteMenu:public CCLayer{
public:
	SpriteMenu();
	 ~SpriteMenu();
	void menuCallback(CCObject * pSender);
    void closeCallback(CCObject * pSender);
private:
    CCPoint m_tBeginPos;
    CCMenu* m_pItemMenu;
}