#include "CallMethod.h"
#include <string>
#include "AlertMessage.h"
#include "StringMenu.h"
using namespace std;
using namespace cocos2d;

CallMethod::CallMethod(){
}
CallMethod::~CallMethod(){
}

void CallMethod::method1(){
	CCScene *pScene = CCScene::create();
	CCDirector *pDirector = CCDirector::sharedDirector();
	//pDirector->purgeCachedData();
	pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());

	  CCSize size = CCDirector::sharedDirector()->getWinSize();
	//StringMenu* pMenu =new StringMenu();
	StringMenu* pMenu=new StringMenu(
		CCPoint(size.width/3 , size.height/3),
		30,
		40,
		StringMenu::STRING_MENU_STYLE_VERTICAL
		);
	pMenu->autorelease();
	//给要转到的场景增加一个菜单
	StringMenuItem item1 = StringMenuItem("second item1" , sceneSecondMethod1);
	pMenu->addMenuItem(item1);
	pMenu->update();
	pScene->addChild(pMenu,102);

	    // 创建图片精灵  
    CCSprite* pSprite = CCSprite::create("HelloWorld.png");      
    // 设置图片精灵的位置  
    pSprite->setPosition(ccp(size.width/2, size.height/2));  

	pScene->addChild(pSprite , 101);
	CCTransitionFadeDown* pFadeDown =CCTransitionFadeDown::create(0.5f , pScene);
	//转到这个场景
	pDirector->replaceScene(pFadeDown);
}

void CallMethod::sceneSecondMethod1(){
	//MessageBox(0 , L"secondMenuItem" , L"abc" ,0);
	
	CCScene *pScene = CCScene::create();
	CCDirector *pDirector = CCDirector::sharedDirector();
	//pDirector->purgeCachedData();
	pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());

	  CCSize size = CCDirector::sharedDirector()->getWinSize();
	//StringMenu* pMenu =new StringMenu();
	StringMenu* pMenu=new StringMenu(
		CCPoint(size.width/4 , size.height/4),
		30,
		40,
		StringMenu::STRING_MENU_STYLE_VERTICAL
		);
	pMenu->autorelease();

	// 创建图片精灵  
    CCSprite* pSprite = CCSprite::create("background.png");      
    // 设置图片精灵的位置  
    pSprite->setPosition(ccp(size.width/2, size.height/2));  
	pScene->addChild(pSprite , 101);

	//给要转到的场景增加一个菜单
	StringMenuItem item1 = StringMenuItem("second item2" , method1);
	pMenu->addMenuItem(item1);
	pMenu->update();
	pScene->addChild(pMenu , 102);

	pDirector->setDepthTest(true);
	CCTransitionFade* pFade = CCTransitionFade::create(0.8f,pScene );
		//转到这个场景
	pDirector->replaceScene(pFade);


};