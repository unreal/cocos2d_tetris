#include "ActionTest.h"
#include "StringMenu.h"
#include "StringMenuItem.h"
#include "AlertMessage.h"
#include "TraceEX.h"
#include "Convert.h"
using namespace cocos2d;
ActionTest::ActionTest(){
}
ActionTest::~ActionTest(){
}

void ActionTest::showScene(){
	
	CCDirector *pDirector = CCDirector::sharedDirector();
	pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
	CCSize size = pDirector->getWinSize();
	CCScene * pScene = CCScene::create();
	CCSprite* pFlag =CCSprite::create("flag.png");
	pFlag->setPosition(ccp(50,size.height - 50));
	pScene->addChild(pFlag);
	CCLayer* pLayer = CCLayer::create();
	pScene->addChild(pLayer);
	pDirector->replaceScene(pScene);

		StringMenu * actionMenu =new StringMenu(
		CCPoint(size.width*0.8 ,size.height*0.9),
		25,
		10,
		StringMenu::STRING_MENU_STYLE_VERTICAL);
		actionMenu->autorelease();
		auto instantMethod = std::bind(ActionTest::actionInstant ,pLayer);
		StringMenuItem item1 = StringMenuItem("action Instant" , instantMethod);
		auto intervalMethod = std::bind(ActionTest::actionInterval ,pLayer);
		StringMenuItem item2 = StringMenuItem("action Interval" , intervalMethod);
		auto easelMethod = std::bind(ActionTest::actionEase ,pLayer);
		StringMenuItem item3 = StringMenuItem("action Ease" , easelMethod);
		auto followMethod = std::bind(ActionTest::actionFollow ,pScene , pLayer);
		StringMenuItem item4 = StringMenuItem("action Follow" , followMethod);
		auto callbackMethod = std::bind(ActionTest::actionCallBack , pLayer);
		StringMenuItem item5 = StringMenuItem("action callBack" , callbackMethod);
		auto callLoad = std::bind(ActionTest::actionLoad , pLayer);
		StringMenuItem item6 = StringMenuItem("action Load" , callLoad);
		auto stop = std::bind(ActionTest::actionManagerStop , pLayer);
		StringMenuItem item7 = StringMenuItem("actionManager:Stop" , stop);
		auto resume = std::bind(ActionTest::actionManagerResume , pLayer);
		StringMenuItem item8 = StringMenuItem("actionManager:resume" , resume);

		actionMenu->addMenuItem(item1);
		actionMenu->addMenuItem(item2);
		actionMenu->addMenuItem(item3);
		actionMenu->addMenuItem(item4);
		actionMenu->addMenuItem(item5);
		actionMenu->addMenuItem(item6);
		actionMenu->addMenuItem(item7);
		actionMenu->addMenuItem(item8);
		actionMenu->update();
		pScene->addChild(actionMenu);
}
//瞬时动作
void ActionTest::actionInstant(CCLayer* pLayer){
	
	pLayer->removeAllChildrenWithCleanup(true);
	CCSize size = CCDirector::sharedDirector()->getWinSize();
		CCSprite* pFlag =CCSprite::create("xiaoguai/11.png");
		pFlag->setPosition(ccp(size.width/2 , size.height/2));
		pLayer->addChild(pFlag);

		CCFlipX* actFlipX = CCFlipX::create(true);
		pFlag->runAction(actFlipX);

}
//延时动作
void ActionTest::actionInterval(CCLayer* pLayer){
		pLayer->removeAllChildrenWithCleanup(true);
	CCSize size = CCDirector::sharedDirector()->getWinSize();
		//移动动作
		CCSprite* pFlag =CCSprite::create("xiaoguai/11.png");
		pFlag->setPosition(ccp(size.width*0.5 , size.height*0.5));
		pLayer->addChild(pFlag);
		CCMoveTo* actMoveTo = CCMoveTo::create(2.f , ccp(size.width*0.8,size.height*0.8));
		pFlag->runAction(actMoveTo);


		//贝塞尔曲线
		CCSprite* pFlag2 =CCSprite::create("xiaoguai/11.png");
		pFlag2->setPosition(ccp(300,300));
		pLayer->addChild(pFlag2);
		ccBezierConfig bConfig = ccBezierConfig();
		bConfig.controlPoint_1=ccp(300.f,300,f);
		bConfig.controlPoint_2=ccp(500.f,500,f);
		bConfig.endPosition=ccp(400.f , 300.f);
		CCBezierBy* actBezierBy = CCBezierBy::create(2.f, bConfig);
		pFlag2->runAction(actBezierBy);

		//跳跃动画
		CCSprite* pFlag3 =CCSprite::create("xiaoguai/12.png");
		CCSprite* pFlag4 =CCSprite::create("xiaoguai/13.png");
		CCSprite* pFlag5 =CCSprite::create("xiaoguai/14.png");
		pLayer->addChild(pFlag3);
		pLayer->addChild(pFlag4);
		pLayer->addChild(pFlag5);
		pFlag3->setPosition(ccp(200,200));
		pFlag4->setPosition(ccp(300,200));
		pFlag5->setPosition(ccp(400,200));
		  //创建跳跃的动作  
        CCActionInterval*  actionTo = CCJumpTo::create(5, ccp(200,200), 50, 10);  
        CCActionInterval*  actionBy = CCJumpBy::create(5, ccp(0,0), 50, 10);  
        CCActionInterval*  actionUp = CCJumpBy::create(5, ccp(400,200), 80, 10);  
        CCActionInterval*  actionByBack = actionBy->reverse();  


		   //为精灵执行动作  
        pFlag3->runAction( actionTo);  
      //  pFlag4->runAction( CCSequence::create(actionTo , NULL));  
        pFlag5->runAction( actionUp);


		

}
//变速运动
void ActionTest::actionEase(CCLayer* pLayer){
	pLayer->removeAllChildrenWithCleanup(true);
	CCSprite* pFlag1 =CCSprite::create("xiaoguai/11.png");
	pLayer->addChild(pFlag1);
	pFlag1->setTag(1020);
	pFlag1->setPosition(ccp(100 ,600));
	CCActionInterval* move = CCMoveBy::create(3, ccp(500,0));
    CCActionInterval* move_back = move->reverse();

	CCActionInterval* moveIn= CCEaseIn::create((CCActionInterval*)(move->copy()->autorelease()), 3.5f);  
	CCActionInterval* moveOut= CCEaseIn::create((CCActionInterval*)(move_back->copy()->autorelease()), 3.5f);  
  
	
	CCActionInterval* moveDown = CCMoveBy::create(2 , ccp(0 , -300));

	CCSprite* pFlag2 =CCSprite::create("xiaoguai/12.png");
	pFlag2->setPosition(ccp(100 ,500));
	pLayer->addChild(pFlag2);
	//改变动作对象 

	CCTargetedAction *t1 = CCTargetedAction::create(pFlag2, moveDown);
	CCSequence* seq1 = CCSequence::create(moveIn,t1, moveOut, NULL);
	CCAction* speedSeq = CCSpeed::create(seq1,5.f);
//	CCSequence* seq2= CCSequence::create(moveIn, moveOut, NULL);

	pFlag1->runAction(seq1);
}
//跟随动作
void ActionTest::actionFollow(CCScene* pScene ,CCLayer* pLayer){
	pLayer->removeAllChildrenWithCleanup(true);
	CCSprite* pFlag1 =CCSprite::create("xiaoguai/11.png");
	pLayer->addChild(pFlag1);
	pFlag1->setPosition(ccp(100 ,600));
	CCActionInterval* move = CCMoveBy::create(3, ccp(500,0));
    CCActionInterval* move_back = move->reverse();

	CCActionInterval* moveIn= CCEaseIn::create((CCActionInterval*)(move->copy()->autorelease()), 3.5f);  
	CCActionInterval* moveOut= CCEaseIn::create((CCActionInterval*)(move_back->copy()->autorelease()), 3.5f);  
    CCSequence* seq1 = CCSequence::create(moveIn, moveOut, NULL);
	pFlag1->setZOrder(1001);
	pFlag1->runAction(seq1);

	CCAction* followAction = CCFollow::create(pFlag1);
	CCSprite* pFlag2 =CCSprite::create("xiaoguai/18.png");
	pFlag2->setZOrder(1010);
	pLayer->addChild(pFlag2);
	pFlag2->setPosition(ccp(100 ,500));
	pScene->runAction(followAction);
}

//动作回调方法
void ActionTest::actionCallBack(CCLayer* pLayer){
	pLayer->removeAllChildrenWithCleanup(true);

	CCSprite* pFlag1 =CCSprite::create("xiaoguai/11.png");
	pLayer->addChild(pFlag1);
	pFlag1->setPosition(ccp(200 ,600));
	CCActionInterval* move = CCMoveBy::create(3, ccp(500,0));
    CCActionInterval* move_back = move->reverse();
	CCActionInterval* rotate = CCRotateBy::create( 2,90);
	
	ActionTest actionT = ActionTest();

   CCFiniteTimeAction*  action = CCSequence::create(
       rotate,  
        CCCallFunc::create(pLayer, callfunc_selector(ActionTest::callback1)), NULL);  

	pFlag1->runAction(action);
}

void ActionTest::callback1(){
	AlertMessage::alert("a" , "b");
}
//载入动画
void ActionTest::actionLoad(CCLayer* pLayer){
	pLayer->removeAllChildrenWithCleanup(true);

	  CCProgressTo *to1 = CCProgressTo::create(2, 100);  
      CCProgressTo *to2 = CCProgressTo::create(2, 100);  
	  CCSize size = CCDirector::sharedDirector()->getWinSize();
        CCProgressTimer *left = CCProgressTimer::create(CCSprite::create("xiaoguai/14.png"));  
        left->setType( kCCProgressTimerTypeBar );  
        pLayer->addChild(left);  
        left->setPosition(ccp(size.width*0.4, size.height/2));  
        left->runAction( CCRepeatForever::create(to1));  
		left->setBarChangeRate(ccp(30,0));
        CCProgressTimer *right = CCProgressTimer::create(CCSprite::create("xiaoguai/11.png"));  
        right->setType(kCCProgressTimerTypeRadial);  
        // Makes the ridial CCW  
        right->setReverseProgress(true);  
        pLayer->addChild(right);  
        right->setPosition(ccp(size.width*0.6, size.height/2));  
        right->runAction( CCRepeatForever::create(to2));  
}
//动作管理类 暂停
void ActionTest::actionManagerStop(CCLayer* pLayer){
	CCSprite* pFlag =(CCSprite*) pLayer->getChildByTag(1020);
	 CCDirector* pDirector = CCDirector::sharedDirector();  
     //pDirector->getActionManager()->pauseTarget(pFlag);
	 pDirector->getActionManager()->pauseAllRunningActions();
}
//恢复
void ActionTest::actionManagerResume(CCLayer* pLayer){
	CCSprite* pFlag =(CCSprite*) pLayer->getChildByTag(1020);
	 CCDirector* pDirector = CCDirector::sharedDirector();  
	 //pDirector->getActionManager()->resumeTarget(pFlag);
	 
	 
	 CCArray* children = pLayer->getChildren();
	 if(children!=NULL){
		for(int ix = 0 ; ix<children->count() ; ix++){
			 std::string count=Convert::ToString(ix);
			 TraceEX::Trace("children:" ,count);
			 pDirector->getActionManager()->resumeTarget(children->objectAtIndex(ix));
		 }
	 }


}

