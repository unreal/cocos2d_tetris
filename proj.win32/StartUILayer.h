#pragma once
#include "cocos2d.h"
class StartUILayer : public cocos2d::CCLayer{
public:
	StartUILayer();
	~StartUILayer();
	CREATE_FUNC(StartUILayer);
	bool init();
	void initMenu();
private:
	void static startGameScene();
};