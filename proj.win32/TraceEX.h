#pragma once
#include <Windows.h>
#include <stdio.h>
#include <iostream>
/**
** log输出类
**/
class TraceEX{
public:
		inline static void TraceEx(LONGLONG obj,const char* fmt){
		char c[1024];
		sprintf_s(c,fmt,obj);
		OutputDebugStringA(c);
	}

	inline static void TraceExFloat(float obj,const char* fmt){
		char c[1024];
		sprintf_s(c,fmt,obj);
		OutputDebugStringA(c);
	}

	/**
	**打印文字log
	**/
	inline static void Trace(std::string title , std::string info){
		std::string  strAll = title+" "+info;
		if(strAll.size()>=1000){
			strAll.substr(0,950);
			strAll.append("--●﹏●LOG太长●﹏●--");
		}
		strAll.append("\n");
		char c[1024];
		sprintf_s(c , strAll.c_str());
		OutputDebugStringA(c);
	}

};