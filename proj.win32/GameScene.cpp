#include "GameScene.h"
#include "GameBackgroundLayer.h"
#include "GameLayer.h"
#include "ConstantConfig.h"
#include "StringMenu.h"
#include "StringMenuItem.h"
using namespace cocos2d;
CCScene* GameScene::create(){
	CCScene* pScene = CCScene::create();
	GameBackgroundLayer* gameBackLayer = GameBackgroundLayer::create();
	
	pScene->addChild(gameBackLayer,1001);

	GameLayer* gameLayer = GameLayer::create();
	gameLayer->setTag(ConstantConfig::GAMESCENE_GAMELAYER_TAG);
	pScene->addChild(gameLayer,1010);



	return pScene;
}