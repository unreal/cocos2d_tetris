#pragma once
#include "cocos2d.h"
class OrbitCamera{
public:
	OrbitCamera();
	~OrbitCamera();
	void showScene();
private:
	void static orbit(cocos2d::CCScene* pScene);
};