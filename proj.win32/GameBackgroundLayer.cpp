#include "GameBackgroundLayer.h"
#include "ResourcePath.h"
using namespace cocos2d;

GameBackgroundLayer::GameBackgroundLayer(){}
GameBackgroundLayer::~GameBackgroundLayer(){}

bool GameBackgroundLayer::init(){
	CCSize size = CCDirector::sharedDirector()->getWinSize();
	CCSprite* pFlag =CCSprite::create(RES_IMAGE_GAMESCENE_BACKGROUND.c_str());
	pFlag->setPosition(ccp(size.width/2 , size.height/2));
	this->addChild(pFlag);
	return true;
}