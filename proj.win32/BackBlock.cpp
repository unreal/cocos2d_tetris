#include "BackBlock.h"
#include <set>

BackBlock::BackBlock(){}
BackBlock::~BackBlock(){}
bool BackBlock::init(){
	return true;
}
void BackBlock::addBackLine(BackLine* line){
	this->addChild(line); //同步到child里
	this->_listLines.push_back(line);
}
void BackBlock::delBackLine(BackLine* line){
	this->_listLines.remove(line);
	this->removeChild(line);
}


bool BackBlock::isTouchTop(Block* block){
	//只有有一个小方块 ，和一个背景格子小方块 x一致，并且y>1
	std::list<BackLine*>::iterator iter = this->_listLines.begin();
	BackLine* line = nullptr;
	while(iter!=_listLines.end()){
		line =*iter;
		std::list<BlockItem*> lineItems = line->getLineItems();
		std::list<BlockItem*>::iterator lineIter = lineItems.begin();
		BlockItem* lineItem;
		while(lineIter!=lineItems.end()){
			lineItem = *lineIter;
			if(block->isTouchItemBottom(lineItem)){
				return true;
			}
			lineIter++;
		}
		iter++;
	}
	return false;
}

bool BackBlock::isTouchLeft(Block* block){
		std::list<BackLine*>::iterator iter = this->_listLines.begin();
	BackLine* line = nullptr;
	while(iter!=_listLines.end()){
		line =*iter;
		std::list<BlockItem*> lineItems = line->getLineItems();
		std::list<BlockItem*>::iterator lineIter = lineItems.begin();
		BlockItem* lineItem;
		while(lineIter!=lineItems.end()){
			lineItem = *lineIter;
			if(block->isTouchItemRight(lineItem)){//是相对背景格子的左侧碰撞，则调用方块的右碰撞.
				return true;
			}
			lineIter++;
		}
		iter++;
	}
	return false;
}
bool BackBlock::isTouchRight(Block* block){
		std::list<BackLine*>::iterator iter = this->_listLines.begin();
	BackLine* line = nullptr;
	while(iter!=_listLines.end()){
		line =*iter;
		std::list<BlockItem*> lineItems = line->getLineItems();
		std::list<BlockItem*>::iterator lineIter = lineItems.begin();
		BlockItem* lineItem;
		while(lineIter!=lineItems.end()){
			lineItem = *lineIter;
			if(block->isTouchItemLeft(lineItem)){
				return true;
			}
			lineIter++;
		}
		iter++;
	}
	return false;
}



bool BackBlock::isFullBlockLine(){
	return false;
}

std::list<BackLine*> BackBlock::getFullBlockLine(){
	std::list<BackLine*> list;
	return list;
}
void BackBlock::addBlockItem(BlockItem* item){
	//取出所属行
	BackLine* line = this->getBackLine(item->getPosY());
	line->addBlockItem(item);
}

BackLine* BackBlock::getBackLine(int lineSerial){
	std::list<BackLine*>::iterator iter = this->_listLines.begin();
	BackLine* line = nullptr;
	while(iter!=_listLines.end()){
		line =*iter;
		iter++;
	}
	if(line==nullptr){//若为空指针
		line = BackLine::create(lineSerial);
		this->addBackLine(line);
	}

	return line;
}