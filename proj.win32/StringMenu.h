#pragma once
#include "cocos2d.h"
#include "StringMenuItem.h"
#include <functional>


/**
**文字菜单
**/
class StringMenu : public cocos2d::CCLayer
{
public:
	/**
	*@comment 生成一个菜单，使用默认参数赋值
	**/
    StringMenu();
	/**
	*@comment						新生成一个菜单,使用传入参数赋值
	*@param		  beginPos		起始位置
	*@param		  fontSize			菜单字体大小
	*@param		  range				菜单间距【横排竖排效果可能不一样。】
	*@param		  menuStyle		菜单样式，横排还是竖排。
	**/
	StringMenu(cocos2d::CCPoint beginPos , int fontSize , int range , std::string menuStyle);
    ~StringMenu();

	/**
	*@comment 添加一个菜单选项
	*@param   stringMenuItem 菜单选项实体
	**/
	void addMenuItem(StringMenuItem stringMenuItem);

	/**
	*@comment 更新菜单，刷新所有选项
	**/
	void update();

	 static const std::string STRING_MENU_STYLE_VERTICAL;//竖排菜单
	 static const  std::string STRING_MENU_STYLE_HORIZONTAL;//横排菜单

private:
    cocos2d::CCPoint m_BeginPos;//起始位置
	cocos2d::CCMenu* m_pMenu;//cocos2dx菜单类
	//菜单样式
	std::string menuStyle;//菜单形式 纵排还是横排
	std::vector<StringMenuItem> vecMenuList;//选项列表
	//每个选项的使用的属性
	int m_fontSize; //字体大小
	int m_range;//距离 行距或者列距
	int zOrder;//z轴深度
	
private://方法
	 void menuCallback(CCObject * pSender);//用于实现回调，暂不清楚2dx回调如何实现。
};